import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { WSDL_CONTEXTS } from 'src/wsdl/wsdl.dto';
import { WsdlService } from 'src/wsdl/wsdl.service';
import { EsocialMethods } from './esocial.dto';

@Injectable()
export class EsocialService {
  private readonly logger = new Logger(EsocialService.name);
  constructor(
    private readonly configService: ConfigService,
    private readonly wslService: WsdlService,
  ) {}

  private readonly ESOCIAL_ENVIROMENT =
    this.configService.get<string>('ESOCIAL_ENVIROMENT');

  public sendBatchEvents(): string {
    const client = this.wslService.getSoapClient(
      WSDL_CONTEXTS.ESOCIAL_BATCH_SEND_EVENTS_WSDL,
    );

    this.logger.log(`ESOCIAL ENVIROMENT: ${this.ESOCIAL_ENVIROMENT}`);

    try {
      client[EsocialMethods.SEND_BATCH_EVENTS]((err, result) => {
        if (err) {
          this.logger.error(err);
        } else {
          console.log(result);
        }
      });
    } catch (error) {
      this.logger.error(`${EsocialMethods.SEND_BATCH_EVENTS} ERROR: ${error}`);
      throw new BadRequestException('Error on try execute method');
    }

    return 'ok';
  }
}
