import { Controller, Post } from '@nestjs/common';
import { EsocialService } from './esocial.service';

@Controller('events')
export class EsocialController {
  constructor(private readonly esocialService: EsocialService) {}

  @Post('batch')
  public sendBatchEvents() {
    return this.esocialService.sendBatchEvents();
  }
}
