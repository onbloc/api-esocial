import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { WsdlModule } from 'src/wsdl/wsdl.module';
import { WsdlService } from 'src/wsdl/wsdl.service';
import { EsocialController } from './esocial.controller';
import { EsocialService } from './esocial.service';

@Module({
  imports: [ConfigModule, WsdlModule],
  controllers: [EsocialController],
  providers: [ConfigService, EsocialService, WsdlService],
})
export class EsocialModule {}
