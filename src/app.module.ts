import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { configValidationSchema } from './config.schema';
import { EsocialModule } from './esocial/esocial.module';
import { WsdlModule } from './wsdl/wsdl.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      ignoreEnvFile: process.env.STAGE != 'dev',
      envFilePath: ['.env'],
      validationSchema: configValidationSchema,
    }),
    EsocialModule,
    WsdlModule,
  ],
})
export class AppModule {}
