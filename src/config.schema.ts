import * as Joi from '@hapi/joi';

const TTL_1_HOUR = 60 * 60;

export const configValidationSchema = Joi.object({
  PORT: Joi.number().default(3000),
  STAGE: Joi.string().default(''),
  LOG_LEVEL: Joi.string().valid('debug', 'error').default('error'),
  JWT_PRIVATE_KEY: Joi.string().required(),
  JWT_PUBLIC_KEY: Joi.string().required(),
  REDIS_HOST: Joi.string().default('localhost'),
  REDIS_PORT: Joi.string().default(6381),
  REDIS_PASSWORD: Joi.string().default(''),
  REDIS_TTL: Joi.string().default(TTL_1_HOUR),
  // ESOCIAL
  ESOCIAL_ENVIROMENT: Joi.string()
    .valid('RESTRICT_PRODUCTION', 'PRODUCTION')
    .default('RESTRICT_PRODUCTION'),
  ESOCIAL_RESTRICT_PROD_SEND_BATCH_EVENTS_API_URL: Joi.string().required(),
  ESOCIAL_RESTRICT_PROD_CONSULT_BATCH_EVENTS_API_URL: Joi.string().required(),
  ESOCIAL_PROD_SEND_BATCH_EVENTS_API_URL: Joi.string().required(),
  ESOCIAL_PROD_CONSULT_BATCH_EVENTS_API_URL: Joi.string().required(),
  ESOCIAL_CONSTULT_ACTION_API_URL: Joi.string().required(),
  ESOCIAL_SEND_ACTION_API_URL: Joi.string().required(),
  ESOCIAL_BATCH_CONSULTING_EVENTS_WSDL: Joi.string().required(),
  ESOCIAL_BATCH_SEND_EVENTS_WSDL: Joi.string().required(),
  ESOCIAL_EVENTS_CONSULTING_IDENTIFICATION_WSDL: Joi.string().required(),
  ESOCIAL_EVENTS_REQUEST_DOWNLOAD_WSDL: Joi.string().required(),
});
