import { Controller, Get, Response } from '@nestjs/common';
import { WSDL_CONTEXTS } from './wsdl.dto';
import { WsdlService } from './wsdl.service';

@Controller('wsdl')
export class WsdlController {
  constructor(private readonly wsdlService: WsdlService) {}

  @Get('download')
  public requestDownloadWSDL(@Response() res) {
    const wsdl = this.wsdlService.getWsdlXML(
      WSDL_CONTEXTS.ESOCIAL_EVENTS_REQUEST_DOWNLOAD_WSDL,
    );
    res.set('Content-Type', 'text/xml');
    res.send(wsdl);
  }

  @Get('consulting')
  public consultingWSDL(@Response() res) {
    const wsdl = this.wsdlService.getWsdlXML(
      WSDL_CONTEXTS.ESOCIAL_BATCH_CONSULTING_EVENTS_WSDL,
    );
    res.set('Content-Type', 'text/xml');
    res.send(wsdl);
  }

  @Get('send')
  public sendWSDL(@Response() res) {
    const wsdl = this.wsdlService.getWsdlXML(
      WSDL_CONTEXTS.ESOCIAL_BATCH_SEND_EVENTS_WSDL,
    );
    res.set('Content-Type', 'text/xml');
    res.send(wsdl);
  }

  @Get('identification')
  public identificationWSDL(@Response() res) {
    const wsdl = this.wsdlService.getWsdlXML(
      WSDL_CONTEXTS.ESOCIAL_EVENTS_CONSULTING_IDENTIFICATION_WSDL,
    );
    res.set('Content-Type', 'text/xml');
    res.send(wsdl);
  }
}
