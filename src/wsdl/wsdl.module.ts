import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { WsdlService } from './wsdl.service';
import { WsdlController } from './wsdl.controller';

@Module({
  imports: [ConfigModule],
  providers: [ConfigService, WsdlService],
  controllers: [WsdlController],
})
export class WsdlModule {}
