import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as fs from 'fs';
import { WSDL_CONTEXTS } from './wsdl.dto';
import { soap } from 'strong-soap';

export type WSDL_CONTEXT_OPTION = keyof typeof WSDL_CONTEXTS;

const WSDL_FOLDER_PATH = 'wsdl';

@Injectable()
export class WsdlService {
  private readonly logger = new Logger(WsdlService.name);
  constructor(private readonly configService: ConfigService) {}

  private readonly WSDL_FILES = {
    [WSDL_CONTEXTS.ESOCIAL_BATCH_CONSULTING_EVENTS_WSDL]:
      this.configService.get<string>(
        WSDL_CONTEXTS.ESOCIAL_BATCH_CONSULTING_EVENTS_WSDL,
      ),

    [WSDL_CONTEXTS.ESOCIAL_BATCH_SEND_EVENTS_WSDL]:
      this.configService.get<string>(
        WSDL_CONTEXTS.ESOCIAL_BATCH_SEND_EVENTS_WSDL,
      ),

    [WSDL_CONTEXTS.ESOCIAL_EVENTS_CONSULTING_IDENTIFICATION_WSDL]:
      this.configService.get<string>(
        WSDL_CONTEXTS.ESOCIAL_EVENTS_CONSULTING_IDENTIFICATION_WSDL,
      ),

    [WSDL_CONTEXTS.ESOCIAL_EVENTS_REQUEST_DOWNLOAD_WSDL]:
      this.configService.get<string>(
        WSDL_CONTEXTS.ESOCIAL_EVENTS_REQUEST_DOWNLOAD_WSDL,
      ),
  };

  public getWsdlXML(context: WSDL_CONTEXT_OPTION) {
    const filePath = `${WSDL_FOLDER_PATH}/${this.WSDL_FILES[context]}`;

    this.logger.log(`Loading WSDL file "${filePath}"`);

    try {
      const WSDL_FILE = fs.readFileSync(filePath, 'utf8');
      return WSDL_FILE;
    } catch (error) {
      this.logger.log(`WSDL file "${filePath}" not found`);
      throw new BadRequestException('WSDL not found');
    }
  }

  public getSoapClient(context: WSDL_CONTEXT_OPTION): soap.Client {
    const filePath = `${WSDL_FOLDER_PATH}/${this.WSDL_FILES[context]}`;

    return soap.createClient(filePath, (err, client) => {
      if (err) {
        console.log(err);
      } else {
        return client;
      }
    });
  }
}
