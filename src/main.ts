import { LogLevel, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { TransformInterceptor } from './transform.interceptor';

type LogLevelList = LogLevel[];

async function bootstrap() {
  const LOG_LEVEL: LogLevelList =
    process.env.LOG_LEVEL === 'debug' ? ['debug', 'log'] : ['error'];

  const app = await NestFactory.create(AppModule, {
    logger: LOG_LEVEL,
  });

  app.enableCors();

  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalInterceptors(new TransformInterceptor());

  const port = process.env.PORT || 3000;
  await app.listen(port);
}
bootstrap();
