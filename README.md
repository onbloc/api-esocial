# Api Authentication

## Requirements

1. Redis connection
2. A1 Certificate

## Generete jwt token rsa

1. Generate de private key:

```bash
openssl genrsa -out key.pem 2048
```

2. Generate de public key from private key:

```bash
openssl rsa -in key.pem -outform PEM -pubout -out public.pem
```

note: key.pem is the file name of the private key

## Installation

```bash
yarn
```

## Copy the environment variables file and adjust their values

```bash
cp -r .env.stage.dev.example .env.stage.dev
```

## Running the app

```bash
# development
yarn start

# watch mode
yarn start:dev

# production mode
yarn start:prod
```

## Test

```bash
# unit tests
yarn test

# e2e tests
yarn test:e2e

# test coverage
yarn test:cov
```